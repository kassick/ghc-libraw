{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module LibRaw (getWidth, getHeight, readData)

where

import System.Info
import qualified Language.C.Inline.Cpp as C
import qualified Data.Vector.Storable as V
import           Data.Vector.Storable ( (!) )
import qualified Data.Vector.Storable.Mutable as VM
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as C8
import           Data.Monoid ((<>))
import           Foreign.C.Types
import           Foreign.Ptr
import           Foreign.Storable
import           Foreign.Marshal.Array
import           Foreign.ForeignPtr (newForeignPtr_)

C.context (C.cppCtx <> C.vecCtx <> C.funCtx <> C.bsCtx)

C.include "<libraw/libraw.h>"
C.include "<cstdlib>"
C.include "<string>"

getWidth :: BS.ByteString -> IO C.CUShort
getWidth fname =
  [C.block| unsigned short {

    std::string cfname($bs-ptr:fname, $bs-len:fname);
    LibRaw lr;
    int ret = lr.open_file(cfname.c_str()) ;
    if (ret != LIBRAW_SUCCESS) {
        fprintf(stderr,"Cannot open %s: %s\n", cfname.c_str(), libraw_strerror(ret));
        return -1;
    }

    return lr.imgdata.sizes.iwidth;
                }
  |]


getHeight :: BS.ByteString -> IO C.CUShort
getHeight fname =
  [C.block| unsigned short {

    std::string cfname($bs-ptr:fname, $bs-len:fname);
    LibRaw lr;
    int ret = lr.open_file(cfname.c_str()) ;
    if (ret != LIBRAW_SUCCESS) {
        fprintf(stderr,"Cannot open %s: %s\n", cfname.c_str(), libraw_strerror(ret));
        return -1;
    }

    return lr.imgdata.sizes.iheight;
                }
  |]

readData :: BS.ByteString         -- fname
               -> IO (V.Vector C.CUShort, -- Red channel
                      V.Vector C.CUShort, -- Green channel
                      V.Vector C.CUShort, -- Blue channel
                      V.Vector C.CUShort) -- Green2 channel
readData fname = do
  width <- getWidth fname
  height <- getHeight fname
  let npixels = (fromIntegral width) * (fromIntegral height)
  let autoscale = 1
      black_subtraction = 0
      use_gamma = 0
  vred <- VM.replicate npixels (0 :: C.CUShort)
  vgreen <- VM.replicate npixels (0 :: C.CUShort)
  vblue <- VM.replicate npixels (0 :: C.CUShort)
  vgreen2 <- VM.replicate npixels (0 :: C.CUShort)
  -- VM.set vred 1
  -- print "Npixels: "
  -- print npixels

  ret <- [C.block| int {
    std::string cfname($bs-ptr:fname, $bs-len:fname);
    LibRaw lr;

    // Not sure if this makes any difference if we don't export
    if(!$(int use_gamma))
        lr.imgdata.params.gamm[0] = lr.imgdata.params.gamm[1] = 1;

    int ret = lr.open_file(cfname.c_str()) ;
    if (ret != LIBRAW_SUCCESS) {
        fprintf(stderr,"Cannot open %s: %s\n", cfname.c_str(), libraw_strerror(ret));
        return -1;
    }

    if(lr.imgdata.idata.is_foveon)
    {
        printf("Cannot process Foveon image %s\n", cfname.c_str());
        return -1;
    }

    ret = lr.unpack();
    if (ret != LIBRAW_SUCCESS)
    {
        fprintf(stderr,"Cannot unpack %s: %s\n", cfname.c_str(), libraw_strerror(ret));
        return -1;
    }

    lr.raw2image();

    auto width = lr.imgdata.sizes.iwidth;
    auto height = lr.imgdata.sizes.iheight;

    if($(int black_subtraction))
        lr.subtract_black();

    if($(int autoscale))
    {
        unsigned max=0,
                scale=1;
        for(int j=0; j < height*width; j++)
            for(int c = 0; c < 4; c++)
                if(max < lr.imgdata.image[j][c])
                    max = lr.imgdata.image[j][c];

        if (max >0 && max< 1<<15)
        {
            scale = (1<<16)/max;
            printf("Scaling with multiplier=%d (max=%d)\n", scale, max);
            for(int j=0; j < height * width; j++)
                for(int c = 0 ; c < 4 ; c++)
                    lr.imgdata.image[j][c] *= scale;
        }

        printf("Black level (scaled)=%d\n", lr.imgdata.color.black * scale);
    }
    else
        printf("Black level (unscaled)=%d\n", lr.imgdata.color.black);

    for (int i = 0; i < height * width; i++)
    {
       //printf("%d -- %u ", i, lr.imgdata.image[i][0]);
       $vec-ptr:(unsigned short *vred)[i] = lr.imgdata.image[i][0];
       $vec-ptr:(unsigned short *vgreen)[i] = lr.imgdata.image[i][1];
       $vec-ptr:(unsigned short *vblue)[i] = lr.imgdata.image[i][2];
       $vec-ptr:(unsigned short *vgreen2)[i] = lr.imgdata.image[i][3];
    }

    return 0;
  } |]

  vr <- V.freeze vred
  vg <- V.freeze vgreen
  vb <- V.freeze vblue
  vg2 <- V.freeze vgreen2

  return (vr, vg, vb, vg2)

someFunc :: IO ()
someFunc = do
  putStrLn "someFuncsdasds"
  print System.Info.compilerVersion
  w <- getWidth $ C8.pack "teste.NEF"
  h <- getHeight $ C8.pack "teste.NEF"
  print w
  print h
  (vr, vg, vb, vg2) <- readData $ C8.pack "teste.NEF"

  print $ vr ! 0
  print $ vr ! 1
  print $ vr ! 2
  print $ vr ! 3
  print $ vr ! (fromIntegral $ h*w - 4)
  print $ vr ! (fromIntegral $ h*w - 3)
  print $ vr ! (fromIntegral $ h*w - 2)
  print $ vr ! (fromIntegral $ h*w - 1)

  print "end"
