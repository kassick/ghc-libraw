# ghc-LibRaw

A **very** bad wrapper around LibRaw. It currently receives a file name and reads the R/G/B/G2 channels inside a Data.Vector.Storable.

See app/Main.hs for an example

## Compilation

```sh
stack build
```
