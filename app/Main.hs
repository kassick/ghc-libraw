module Main where

import qualified LibRaw as LR
import qualified Data.ByteString.Char8 as C8
import           Data.Vector.Storable ( (!) )
import qualified Data.Vector.Storable as V

main :: IO ()
main = do
  w <- LR.getWidth $ C8.pack "teste.NEF"
  h <- LR.getHeight $ C8.pack "teste.NEF"
  (vr, vg, vb, vg2) <- LR.readData $ C8.pack "teste.NEF"

  print $ vr ! 0
  print $ vr ! 1
  print $ vr ! 2
  print $ vr ! 3
  print $ vr ! (fromIntegral $ h*w - 4)
  print $ vr ! (fromIntegral $ h*w - 3)
  print $ vr ! (fromIntegral $ h*w - 2)
  print $ vr ! (fromIntegral $ h*w - 1)
